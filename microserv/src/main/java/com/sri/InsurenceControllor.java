package com.sri;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InsurenceControllor {
	
	
	@Autowired
	InsurenceRep insurenceRep;
	
	@PostMapping("/add")
	public ResponseEntity<Insurence> add(@RequestBody Insurence insurence ){
	insurenceRep.save(insurence);
		
		
		return ResponseEntity.ok(insurence);
	}
	@GetMapping("/get/{id}")
	public ResponseEntity<Insurence> getbyid(@PathVariable Long id  ){
	Optional<Insurence>	 insurence	=insurenceRep.findById(id);
	
	if(insurence.isPresent())
		return ResponseEntity.ok( insurence.get());
		else
			 return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/gtall")
	public ResponseEntity<List<Insurence>> add(){
	List<Insurence> insurencelist=insurenceRep.findAll();
		
		
		return ResponseEntity.ok(insurencelist);
	}
	
	
	
		
	
		
	
	
	
	
	
}
