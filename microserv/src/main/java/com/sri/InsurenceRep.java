package com.sri;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InsurenceRep extends JpaRepository<Insurence, Serializable> {

}
