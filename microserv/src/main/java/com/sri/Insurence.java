package com.sri;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="insurence table ")
public class Insurence {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	public Long insure;
	public String name ;
	public String Adress;
}
